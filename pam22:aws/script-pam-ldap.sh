# Script per automatització del procés d'autenticació d'usuaris LDAP contra un host
#
# Instal·lació de packets necessaris

apt-get update
apt-get -y libpam-ldap libnss-ldapd libpam-mount nslcd

# Edició de fitxers

echo -e "BASE\tdc=edt,dc=org\nURI\tldap://ldap.edt.org" >> /etc/ldap/ldap.conf
sed -ri "s/^(passwd:)(.*)/\1 \ \tfiles ldap/" /etc/nsswitch.conf
sed -ri "s/^(passwd:)(.*)/\1 \ \tfiles ldap/" /etc/nsswitch.conf
sed -ri "s/^(base)(.*)/\1\ dc=edt,dc=org/" /etc/nslcd.conf
sed -ri "s/^(uri)(.*)/\1\ ldap:\/\/ldap.edt.org/" /etc/nslcd.conf

# Engegar serveis

systemctl start nscd
systemctl start nslcd
