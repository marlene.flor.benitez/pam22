### PRACTICA 8 - PAM_MOUNT
- A tots els usuaris es munta dins el seu home un recurs anomenat tmp de 100M
corresponent a un ramdisk tmpfs.
- Només a l’usuari unix01 es munta dins el seu home un recurs anomenat tmp de
200M corresponent a un ramdisk tmpfs.
- A l’usuari unix02 se li munta dins el home un recurs NFS de xarxa.
*Nota* Creeu un recurs de xarxa NFS per exemple /usr/share/doc exportat per NFS.

> Com el que volem és que quan un usuari inici *sessió* es crei el seu home dir hem d'utilitzar el modul pam_mkhomedir i hem de possar-ho en el **common-session**, ja que el fitxer de login importa aquest, a més de que crear el homedir del usuari és per qualsevol usuari.

```
session		required	pam_mkhomedir.so
```

> Una vegada els home están creats, procedem a montar els directoris tmp i el dir nfs compartit. Per aixó hem d'editar el fitxer **/etc/security/pam_mount.conf.xml** i afegir cada volum amb les característiques que ens interesa. A més hem d'afegir una línea al **/estc/pam.d/common-session** per tal d'utilizar el modul de pam_mount.

```
session		required	pam_mount.so
```
> **Tots els usuaris**

```
<volume
		user="*"
		fstype="tmpfs"
		options="size=100M"
		mountpoint="~/tmp"
	/>
```

> **Usuari unix01**

```
<volume
		user="unix01"
		fstype="tmpfs"
		options="size=200M"
		mountpoint="~/tmp"
	/>
```

> **Usuari unix02**
> Per fer la part de nfs primerament hem d'engegar el servidor nfs en el nostre ordinador. Hem de descargar-ho, *nfs-server*, engegar ho amb systemctl i editar el fitxer **/etc/exports** per tal d'indicar quins fitxers estem compartint. I en el container instalar el paquet client *nfs-common*.

* /etc/exports (en el propi ordinador)
```
/usr/share/doc	*(ro,sync)
/usr/share/man	*(ro,sync)
```
* /etc/security/pam_mount.conf.xml (en el container)
```
<volume
		user="unix02"
		fstype="nfs"
		server="g15"
		path="/usr/share/man"
		mountpoint="~/man"
	/>
```

- Comprovació
```
unix02@pam:~$ df -h
Filesystem          Size  Used Avail Use% Mounted on
overlay              89G   23G   62G  28% /
tmpfs                64M     0   64M   0% /dev
shm                  64M     0   64M   0% /dev/shm
/dev/sda5            89G   23G   62G  28% /etc/hosts
none                200M     0  200M   0% /home/unix01/tmp
none                100M     0  100M   0% /home/prova1/tmp
none                100M     0  100M   0% /home/prova/tmp
g15:/usr/share/man   89G   23G   62G  28% /home/unix02/man
none                100M     0  100M   0% /home/unix02/tmp
```
```
unix02@pam:~$ mount | tail
shm on /dev/shm type tmpfs (rw,nosuid,nodev,noexec,relatime,size=65536k)
/dev/sda5 on /etc/resolv.conf type ext4 (rw,relatime,errors=remount-ro)
/dev/sda5 on /etc/hostname type ext4 (rw,relatime,errors=remount-ro)
/dev/sda5 on /etc/hosts type ext4 (rw,relatime,errors=remount-ro)
devpts on /dev/console type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=666)
none on /home/unix01/tmp type tmpfs (rw,relatime,size=204800k,gid=1000)
none on /home/prova1/tmp type tmpfs (rw,relatime,size=102400k,gid=1006)
none on /home/prova/tmp type tmpfs (rw,relatime,size=102400k,gid=1005)
g15:/usr/share/man on /home/unix02/man type nfs4 (rw,relatime,vers=4.2,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=172.18.0.2,local_lock=none,addr=10.200.245.215)
none on /home/unix02/tmp type tmpfs (rw,relatime,size=102400k,gid=1001)
```
