# PAM

> Pluggable Authentication Modules **(PAM)** --- 

```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it marleneflor/pam22:base
```

```
docker build -t marleneflor/pam22:base .
```
- auth --- autorització
- account --- iniciar sessio
- password --- canviar el passwd
- session --- coses que es fan durant i al terminar la sessio

* Exemples

```
auth		optional	pam_echo.so [ missatge user %u ]
auth		optional	pam_echo.so [ host %h service %s ]
#auth		sufficient	pam_unix.so
#auth		required	pam_permit.so
#auth		required	pam_unix.so
#auth		required	pam_succeed_if.so debug uid >= 1000
#auth		required 	pam_succeed_if.so user = unix01

auth		sufficient	pam_deny.so
auth		required	pam_unix.so
account		sufficient	pam_permit.so
```

> Existeixen les plantilles generiques, els common en el directori /etc/pam.d. La majoria de fitxers de definició de regles.
> cada programa te el seu fitxer de definicio de regles qu epot fer include d'altres
> en cas de no tenir-ne fitxer de definicio de regles, es fa servir el 'other'

- PRACTICA 8 PAM MOUNT

> NFS (Network File System) --- protocol per recursos compartits de xarxa

mount -t nfs server:/recurs/a/desti /mnt/ (mount point) 100M

- instalar client
```
sudo apt-get install nfs-common
```
- instalar servidor
```
sudo apt-get install nfs-server
```
- /etc/export --- fitxer que conte les coses que han d'exportar-se

- port 2049 --- port del servei nfs

