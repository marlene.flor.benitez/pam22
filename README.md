# PAM22

## @Marlene Flor - ASIX M06 - 2022-23

### Containers

- **pam22:base** --- container pam base per practicar regles PAM, aprendre el funcionament dels type (auth,account,session ipassword) i dels control bàsics (sufficient, required,requisite i optional). A més de la practica 7 (pam_time.so) i practica 8 (pam_mount.so).

```
docker build -t marleneflor/pam22:base .
docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisx -it marleneflor/pam22:base
```

- **pam22:ldap** --- container per practicar l'autenticació d'usuaris LDAP amb PAM i aplicar també la practica 8 amb el modul pam_mount.so

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d marleneflor/ldap22:latest
docker run --rm --name pam.edt.org  -h pam.edt.prg  --net 2hisx --privileged -it marleneflor/pam22:ldap
```

- **pam22:python** --- container pam per practicar amb PAM Aware (pamaware.py) i crear un modul PAM propi(pam_mates.py)  amb python.
 El pamaware.py que és un programa que utilitza el modul pam_unix per mostrar numeros del 1 al 10 si l'usuari introduit és un usuari autenticat.
 El pam_mates.py és un modul PAM creat amb python i utilitzant el modul pam_python.so, que permet fer el chfn d'un usuari si aquest sap quant és 3 * 2.

```
docker build --no-cache -t marleneflor/pam22:python .
docker run --rm --name pam.edt.org  -h pam.edt.prg  --net 2hisx --privileged -it marleneflor/pam22:python
```
