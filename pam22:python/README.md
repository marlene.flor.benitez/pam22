# PAM22:PYTHON

- Crear un modul pam anomenat pam_mates.py que per validar un usuari li farà una
pregunta de matemàtiques, si la respon bé pot accedir al servei. Caldrà usar el mòdul
pam_python.so que permet executar mòduls escrits en python.
- S’aplicarà el mòdul al servei chfn de manera que l’usuari es podrà canviar el finger si sap
matemàtiques.

### Crear imatge

```
docker build --no-cache -t marleneflor/pam22:python .
```

### Engegar container

```
docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisx --privileged -it marleneflor/pam22:python
```

### Fragment d'autenticació del pam_mates.py

```
def  pam_sm_authenticate (pamh, flags, argv):
    print("Quant fan 3*2?")
    resposta=int(input())
    if int(resposta) == 6:
        return pamh.PAM_SUCCESS
    else:
        return pamh.PAM_PERM_DENIED
```
> Amb la funció pam_sm_authenticate posem les condicions d'autenticació del nostre modul PAM. En aquest cas fem una pregunta, 'Quant fan 3 * 2?', i segons si la resposta és correcta o no, es retorna success (PAM_SUCCESS) o fail (PAM_PERM_DENIED)

### Configuració del chfn

```
#%PAM-1
#-*- coding: utf-8-*-
auth 		optional 	pam_echo.so [ auth ----------------- ]
auth 		required 	pam_python.so /opt/docker/pam_mates.py
account 	optional 	pam_echo.so [ account -------------- ]
account 	sufficient 	pam_python.so /opt/docker/pam_permit.py
password 	include 	pam_deny.so
session 	include 	pam_deny.so
```
> **IMPORTANT**: el pam_python.so amb el nostre modul d'autenticació ha de ser required perquè es pugui fer correctament la validació.

